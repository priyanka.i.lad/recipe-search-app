import React from "react";
import { Link } from "react-router-dom";
import "./Header.css";

class Header extends React.Component {
  componentDidMount() {
    if (localStorage.getItem("access-token"))
      this.props.handleLoginStatus(true);
    else this.props.handleLoginStatus(false);
  }

  handleLogout = () => {
    localStorage.clear();
    this.props.handleLoginStatus();
  };

  render() {
    if (this.props.isLoggedIn) {
      return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light pt-0">
          <Link className="navbar-brand title" to="/">
            Foodies
          </Link>
          <div className="justify-content-end ml-auto" id="navbarNav">
            <ul className="navbar-nav">
              <li className="p-2 text-secondary">
                {" "}
                <span>{`Welcome, ${localStorage.getItem("username")}`}</span>
              </li>
              <li className="nav-item active">
                <div className="dropdown">
                  <button className="dropbtn">My Account</button>
                  <div className="dropdown-content">
                    <Link to="/profile">Profile</Link>
                    <Link to="/favourites">Favourites</Link>
                    <Link to="/likes">Likes</Link>
                    <Link to="/dislikes">Dislikes</Link>
                    <div className="dropdown-divider"></div>
                    <Link className="nav-link" onClick={this.handleLogout}>
                      Logout
                    </Link>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </nav>
      );
    }
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light pt-0">
        <h1 className="navbar-brand title">Foodies</h1>
      </nav>
    );
  }
}

export default Header;
