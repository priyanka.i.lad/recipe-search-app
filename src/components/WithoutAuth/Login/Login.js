import React from "react";
import { Link, Redirect } from "react-router-dom";
import CallAPI from "../../../CallAPI";
import Loading from "../../Loading";

class Login extends React.Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      message: "",
      cssClass: "",
      isLoading: false
    };
  }
  componentDidMount() {
    console.log("clearing local storage");
    localStorage.clear();
  }

  setResponseMessage = (message, cssClass) => {
    this.setState({
      message: message,
      cssClass: cssClass
    });
  };

  setLoading = () => {
    this.setState({
      isLoading: true
    });
  };
  unsetLoading = () => {
    this.setState({
      isLoading: false
    });
  };

  handleSubmit = event => {
    event.preventDefault();
    const form = event.currentTarget;
    form.classList.add("was-validated");
    if (form.checkValidity()) {
      let options = {
        method: "POST",
        url: "/login",
        data: {
          email: this.state.email,
          password: this.state.password
        }
      };
      this.setLoading();
      CallAPI(options)
        .then(data => {
          if (data.status === 200) {
            let {
              token,
              userName,
              likes,
              dislikes,
              favourites
            } = data.data.data;
            localStorage.setItem("access-token", token);
            localStorage.setItem("username", userName);
            localStorage.setItem("likes", likes);
            localStorage.setItem("dislikes", dislikes);
            localStorage.setItem("favourites", favourites);
            this.unsetLoading();
            this.props.handleLoginStatus(true);
          } else {
            throw new Error(data.status);
          }
        })
        .catch(error => {
          this.unsetLoading();
          let message = `${error.response.data.message}`;
          if (message === "Account not verified") {
            message = (
              <div>
                Your account is not verified.
                <br />
                <span className="text-info">
                  Please contact administrator for another verification code
                </span>
              </div>
            );
          }
          this.setResponseMessage(message, "text-danger");
        });
    }
  };
  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  render() {
    if (this.state.isLoading) {
      return <Loading />;
    }
    if (this.props.isLoggedIn) {
      return <Redirect to="/" />;
    }
    return (
      <div>
        <div className="container h-100">
          <div className="row">
            <div className="main-col col-xs-10  offset-xs-2 col-md-7 offset-md-2 col-lg-5 offset-lg-3">
              <div
                className={
                  this.state.cssClass + " font-weight-bolder text-center  pt-3"
                }
              >
                {this.state.message}
              </div>
              <form
                noValidate
                className="needs-validation"
                onSubmit={this.handleSubmit}
              >
                <div className="form-group">
                  <label htmlFor="txt-email">Email</label>
                  <input
                    type="text"
                    name="email"
                    className="form-control"
                    onChange={this.handleChange}
                    required
                  />
                  <div className="invalid-feedback">
                    Please enter your email
                  </div>
                </div>
                <div className="form-group">
                  <label htmlFor="txt-password">Password</label>
                  <input
                    type="password"
                    name="password"
                    className="form-control"
                    onChange={this.handleChange}
                    required
                  />
                  <div className="invalid-feedback">
                    Please enter your password
                  </div>
                </div>

                <div className="form-check">
                  {/* <input
                    type="checkbox"
                    id="chk-remember"
                    className="form-check-input"
                  />
                  <label htmlFor="chk-remember">Remember Me</label> */}
                  <div className="d-inline float-right">
                    <Link to={`/forgotpswd`} className="a-link ">
                      Forgot Password?
                    </Link>
                  </div>
                </div>

                <div className="mt-2">
                  <button type="submit" className="btn btn-info">
                    Login
                  </button>
                </div>
                <div className="mt-2">
                  <Link to={`/register`} className="a-link">
                    Haven't any account? Signup
                  </Link>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
