import React from "react";
import { Link } from "react-router-dom";
import CallAPI from "../../CallAPI";
import Loading from "../Loading";

class ResetPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      code: "",
      newPassword: "",
      confirmPassword: "",
      isLoading: false,
      displayPassResetForm: true
    };
  }
  handleChange = event => {
    let { name, value } = event.target;
    this.setState({
      [name]: value
    });
  };

  setLoading = () => {
    this.setState({
      isLoading: true
    });
  };
  unsetLoading = () => {
    this.setState({
      isLoading: false
    });
  };

  shouldDisplayPassResetForm = display => {
    this.setState({
      displayPassResetForm: display
    });
  };

  handleSubmit = event => {
    event.preventDefault();
    const form = event.currentTarget;
    form.classList.add("was-validated");
    let reset_url = `/resetPwd/${this.state.code}`;
    if (form.checkValidity()) {
      let options = {
        method: "POST",
        url: reset_url,
        data: {
          email: this.props.email,
          newPassword: this.state.newPassword
        }
      };

      this.setLoading();
      CallAPI(options)
        .then(data => {
          if (data.status !== 200) throw new Error(data.status);
          this.unsetLoading();
          let message = (
            <div>
              Password reset successfully <br />
              <Link to={`/login`} className="d-block a-link mt-4 p-3">
                Back to Login
              </Link>
            </div>
          );
          this.shouldDisplayPassResetForm(false);
          this.props.setResponseMessage(message, "text-success");
        })
        .catch(error => {
          this.unsetLoading();
          this.shouldDisplayPassResetForm(true);
          let message = `${error.response.data.message}`;
          this.props.setResponseMessage(message, "text-danger");
        });
    }
  };

  render() {
    if (this.state.isLoading) return <Loading />;
    return (
      <>
        <div
          className={
            this.props.messageCSS + " font-weight-bolder text-center  pt-3"
          }
        >
          {this.props.message}
        </div>
        <form
          noValidate
          className="needs-validation"
          onSubmit={this.handleSubmit}
          style={{
            display: this.state.displayPassResetForm ? "block" : "none"
          }}
        >
          <div className="form-group">
            <label htmlFor="code">Enter the code:</label>
            <input
              type="text"
              name="code"
              value={this.state.code}
              onChange={this.handleChange}
              className="form-control"
              required
            />
            <div className="invalid-feedback">Please enter the code</div>
          </div>
          <div className="form-group">
            <label htmlFor="newPassword">New Password</label>
            <input
              type="password"
              name="newPassword"
              value={this.state.newPassword}
              onChange={this.handleChange}
              className="form-control"
              required
            />
            <div className="invalid-feedback">Please enter new password</div>
          </div>
          <div className="form-group">
            <label htmlFor="confirmPassword">Confirm New Password</label>
            <input
              type="password"
              name="confirmPassword"
              value={this.state.confirmPassword}
              onChange={this.handleChange}
              className="form-control"
              required
            />
            <div className="invalid-feedback">Please confirm the password</div>
          </div>
          <button type="submit" className="btn btn-info">
            Submit
          </button>
          <Link to="/login" className="btn btn-secondary float-right">
            Back to Login
          </Link>
          <button
            type="button"
            className="btn btn-link"
            onClick={this.props.clearState}
          >
            Did not recieved the mail? Try again
          </button>
        </form>
      </>
    );
  }
}
export default ResetPassword;
