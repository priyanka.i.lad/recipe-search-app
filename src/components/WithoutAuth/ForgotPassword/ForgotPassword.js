import React from "react";
import { Link } from "react-router-dom";
import CallAPI from "../../../CallAPI";
import ResetPassword from "../ResetPassword";
import Loading from "../../Loading";

class ForgotPassword extends React.Component {
  constructor() {
    super();
    this.state = {
      email: "",
      message: "",
      messageCSS: "",
      isLinkSent: false,
      isLoading: false
    };
  }

  handleChange = event => {
    let { name, value } = event.target;
    this.setState({
      [name]: value
    });
  };

  clearFields = () => {
    this.setState({
      email: ""
    });
  };

  clearState = () => {
    this.setState({
      message: "",
      messageCSS: "",
      isLinkSent: false
    });
  };

  setResponseMessage = (message, cssColor) => {
    this.setState({
      message: message,
      messageCSS: cssColor
    });
  };

  setLoading = () => {
    this.setState({
      isLoading: true
    });
  };
  unsetLoading = () => {
    this.setState({
      isLoading: false
    });
  };

  handleSubmit = event => {
    event.preventDefault();
    const form = event.currentTarget;
    form.classList.add("was-validated");
    if (form.checkValidity()) {
      let options = {
        method: "POST",
        url: "/forgotPassword",
        data: {
          email: this.state.email
        }
      };
      this.setLoading();
      CallAPI(options)
        .then(data => {
          if (data.status !== 200) throw new Error(data.status);
          this.unsetLoading();
          this.setState({
            message: (
              <>
                <div>{data.data.message}</div>
              </>
            ),
            messageCSS: "text-success",
            isLinkSent: true
          });
        })
        .catch(error => {
          this.unsetLoading();
          let message = `${error.response.data.message}`;
          this.setResponseMessage(message, "text-danger");
        });
    }
  };

  render() {
    if (this.state.isLinkSent) {
      return (
        <>
          <div className="container h-100">
            <div className="row">
              <div className="main-col col-xs-10  offset-xs-2 col-md-7 offset-md-2 col-lg-5 offset-lg-3">
                <ResetPassword
                  message={this.state.message}
                  messageCSS={this.state.messageCSS}
                  email={this.state.email}
                  clearState={this.clearState}
                  setResponseMessage={this.setResponseMessage}
                />
              </div>
            </div>
          </div>
        </>
      );
    } else {
      if (this.state.isLoading) {
        return <Loading />;
      }
      return (
        <div>
          <div className="container h-100">
            <div className="row">
              <div className="main-col col-xs-10  offset-xs-2 col-md-7 offset-md-2 col-lg-5 offset-lg-3">
                <div
                  style={{ display: this.state.message ? "block" : "none" }}
                  className={
                    this.state.messageCSS +
                    " font-weight-bolder text-center  pt-3"
                  }
                >
                  {this.state.message}
                </div>
                <form
                  noValidate
                  className="needs-validation"
                  onSubmit={this.handleSubmit}
                >
                  <div className="form-group">
                    <label htmlFor="txt-email">Email</label>
                    <input
                      type="text"
                      name="email"
                      value={this.state.email}
                      onChange={this.handleChange}
                      className="form-control"
                      required
                    />
                    <div className="invalid-feedback">
                      Please enter your email
                    </div>
                  </div>
                  <div>
                    <button type="submit" className="btn btn-info">
                      Send
                    </button>
                    <div className="text-center d-inline float-right">
                      <Link to={`/login`} className="a-link ">
                        Back to Login
                      </Link>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      );
    }
  }
}

export default ForgotPassword;
