import React from "react";
import { Link } from "react-router-dom";
import CallAPI from "../../../CallAPI";
import Loading from "../../Loading";

class Register extends React.Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      userName: "",
      confirmPassword: "",
      message: "",
      cssClass: "",
      isLoading: false,
      displayForm: true
    };
  }

  setResponseMessage = (message, cssClass) => {
    this.setState({
      message: message,
      cssClass: cssClass
    });
  };

  handleChange = event => {
    let { name, value } = event.target;
    this.setState({
      [name]: value
    });
  };

  clearFields = () => {
    this.setState({
      email: "",
      password: "",
      userName: "",
      confirmPassword: ""
    });
  };

  setLoading = () => {
    this.setState({
      isLoading: true
    });
  };
  unsetLoading = () => {
    this.setState({
      isLoading: false
    });
  };

  shouldAppearDisplayForm = display => {
    this.setState({
      displayForm: display
    });
  };

  handleSubmit = event => {
    event.preventDefault();
    const form = event.currentTarget;
    form.classList.add("was-validated");
    if (form.checkValidity()) {
      let options = {
        method: "POST",
        url: "/register",
        data: {
          userName: this.state.userName,
          email: this.state.email,
          password: this.state.password,
          consumerURLverification: "http://localhost:3000/verifyaccount"
        }
      };
      this.setLoading();
      CallAPI(options)
        .then(data => {
          if (data.status !== 200) throw new Error(data.status);
          this.clearFields();
          this.unsetLoading();

          let message = (
            <div>
              Registration is done successfully <br />
              <span className="text-secondary">
                Verification code is sent to your email address
              </span>
              <Link to={`/login`} className="d-block a-link mt-4 p-3">
                Back to Login
              </Link>
            </div>
          );
          this.shouldAppearDisplayForm(false);
          this.setResponseMessage(message, "text-success");
        })
        .catch(error => {
          this.unsetLoading();
          this.shouldAppearDisplayForm(true);
          let message = `${error.response.data.message}`;
          this.setResponseMessage(message, "text-danger");
        });
    }
  };

  render() {
    if (this.state.isLoading) return <Loading />;
    return (
      <div>
        <div className="container h-100">
          <div className="row">
            <div className="main-col col-xs-10  offset-xs-2 col-md-7 offset-md-2 col-lg-5 offset-lg-3">
              <div
                className={
                  this.state.cssClass + " font-weight-bolder text-center  pt-3"
                }
              >
                {this.state.message}
              </div>
              <form
                noValidate
                className="needs-validation"
                onSubmit={this.handleSubmit}
                style={{ display: this.state.displayForm ? "block" : "none" }}
              >
                <div className="form-group">
                  <label htmlFor="txt-uname">Username</label>
                  <input
                    type="text"
                    name="userName"
                    onChange={this.handleChange}
                    value={this.state.userName}
                    className="form-control"
                    required
                  />
                  <div className="invalid-feedback">
                    Please enter your Username
                  </div>
                </div>
                <div className="form-group">
                  <label htmlFor="txt-email">Email</label>
                  <input
                    type="text"
                    name="email"
                    value={this.state.email}
                    onChange={this.handleChange}
                    className="form-control"
                    required
                  />
                  <div className="invalid-feedback">
                    Please enter your email
                  </div>
                </div>
                <div className="form-group">
                  <label htmlFor="txt-password">Password</label>
                  <input
                    type="password"
                    name="password"
                    value={this.state.password}
                    onChange={this.handleChange}
                    className="form-control"
                    required
                  />
                  <div className="invalid-feedback">
                    Please enter your password
                  </div>
                </div>
                <div className="form-group">
                  <label htmlFor="txt-conf-password">Confirm Password</label>
                  <input
                    type="password"
                    name="confirmPassword"
                    value={this.state.confirmPassword}
                    onChange={this.handleChange}
                    className="form-control"
                    required
                  />
                  <div className="invalid-feedback">
                    Confirm password does not match
                  </div>
                </div>
                <div>
                  <button type="submit" className="btn btn-info">
                    Register
                  </button>
                </div>
                <div className="mt-2">
                  <Link to={`/login`} className="a-link">
                    Already have account? Login
                  </Link>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Register;
