import React from "react";
import { Link } from "react-router-dom";
import CallAPI from "../../CallAPI";
import Loading from "../Loading";

class VerifyAccount extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      code: "",
      message: "",
      cssClass: "",
      displayForm: true,
      isLoading: false
    };
  }

  setResponseMessage = (message, cssClass, displayForm) => {
    this.setState({
      message: message,
      cssClass: cssClass
    });
  };

  handleChange = event => {
    let { name, value } = event.target;
    this.setState({
      [name]: value
    });
  };

  setLoading = () => {
    this.setState({
      isLoading: true
    });
  };
  unsetLoading = () => {
    this.setState({
      isLoading: false
    });
  };

  shouldDisplayForm = display => {
    this.setState({
      displayForm: display
    });
  };

  handleSubmit = event => {
    event.preventDefault();
    const form = event.currentTarget;
    form.classList.add("was-validated");
    if (form.checkValidity()) {
      let options = {
        method: "GET",
        url: `/accVerification/${this.state.code}`
      };
      this.setLoading();
      CallAPI(options)
        .then(data => {
          if (data.status !== 200) throw new Error(data.status);
          this.unsetLoading();
          let message = data.data.message;
          this.shouldDisplayForm(false);
          this.setResponseMessage(message, "text-success");
        })
        .catch(error => {
          this.unsetLoading();
          let message = `${error.response.data.message}`;
          this.shouldDisplayForm(true);
          this.setResponseMessage(message, "text-danger");
        });
    }
  };

  render() {
    if (this.state.isLoading) return <Loading />;
    return (
      <div>
        <div className="container h-100">
          <div className="row">
            <div className="main-col col-xs-10  offset-xs-2 col-md-7 offset-md-2 col-lg-5 offset-lg-3">
              <div
                className={
                  this.state.cssClass + " font-weight-bolder text-center  pt-3"
                }
              >
                {this.state.message}
              </div>
              <form
                noValidate
                className="needs-validation"
                onSubmit={this.handleSubmit}
                style={{ display: this.state.displayForm ? "block" : "none" }}
              >
                <div className="form-group">
                  <label htmlFor="code">Enter the verification code:</label>
                  <input
                    type="text"
                    name="code"
                    value={this.state.code}
                    onChange={this.handleChange}
                    className="form-control"
                    required
                  />
                  <div className="invalid-feedback">
                    Please enter the verification code
                  </div>
                </div>

                <button type="submit" className="btn btn-info">
                  Submit
                </button>
              </form>
              <div className="text-center">
                <Link to="/login" className="btn btn-link  mb-2">
                  Back to Login
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default VerifyAccount;
