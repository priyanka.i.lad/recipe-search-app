import React from "react";

function Actions(props) {
  let {
    recipe_id,
    like,
    dislike,
    favourite,
    likeRecipe,
    disLikeRecipe,
    addToFavourites
  } = props;

  let liked_icon = like
    ? "fa fa-thumbs-up fa-2x like-icon"
    : "fa fa-thumbs-o-up fa-2x like-icon";
  let disliked_icon = dislike
    ? "fa fa-thumbs-down fa-2x ml-2 dislike-icon"
    : "fa fa-thumbs-o-down fa-2x ml-2 dislike-icon";
  let favourite_icon = favourite
    ? "fa fa-heart fa-2x ml-2 favourite-icon"
    : "fa fa-heart-o fa-2x ml-2 favourite-icon";
  return (
    <>
      <span className="float-right">
        <button
          className="btn btn-link p-0"
          onClick={() => likeRecipe(recipe_id)}
        >
          <i
            className={liked_icon}
            aria-hidden="true"
            title="Like this recipe"
          />
        </button>
        <button
          className="btn btn-link p-0"
          onClick={() => disLikeRecipe(recipe_id)}
        >
          <i
            className={disliked_icon}
            aria-hidden="true"
            title="Dislike this recipe"
          />
        </button>
        <button
          className="btn btn-link p-0"
          onClick={() => addToFavourites(recipe_id)}
        >
          {" "}
          <i
            className={favourite_icon}
            aria-hidden="true"
            title="Add to wishlist"
          />
        </button>
      </span>
    </>
  );
}

export default Actions;
