import React from "react";
import { Link } from "react-router-dom";
import "./Profile.css";
import CallAPI from "../../../CallAPI";
import Loading from "../../Loading";

class ViewProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      profile: null
    };
  }

  componentDidMount() {
    let options = {
      method: "POST",
      url: "/users/profile",
      headers: {
        token: localStorage.getItem("access-token")
      }
    };
    CallAPI(options)
      .then(data => {
        if (data.status !== 200) throw new Error(data.status);
        this.setState({
          profile: data.data.data
        });
        let { userName } = data.data.data;
        this.updateUserName(userName);
      })
      .catch(error => {
        this.setState({
          message: `Something went wrong while registration. Error: ${error}`
        });
      });
  }

  updateUserName(userName) {
    if (userName) {
      localStorage.setItem("username", userName);
    }
  }

  render() {
    if (this.state.profile) {
      console.log(localStorage.getItem("userName"));
      let { email, userName, mobile, gender, location } = this.state.profile;
      return (
        <>
          <div className="container ">
            <div className="row">
              <div className="main-col col-sm-12 col-md-8 offset-md-2 col-lg-6 offset-lg-3 p-4">
                <div className="form-group">
                  <label htmlFor="txtEmail">Email: </label>
                  <input
                    type="text"
                    readOnly
                    className="form-control"
                    name="email"
                    value={email}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="txtName">User Name: </label>
                  <input
                    type="text"
                    className="form-control"
                    name="name"
                    value={userName}
                    readOnly
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="txtLocation">Location: </label>
                  <input
                    type="text"
                    className="form-control"
                    name="location"
                    value={location}
                    readOnly
                  />
                </div>

                <div className="form-group">
                  <label htmlFor="txtMobile">Mobile : </label>
                  <input
                    type="text"
                    className="form-control"
                    name="phone"
                    value={mobile}
                    readOnly
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="txtMobile">Gender : </label>
                  <input
                    type="text"
                    className="form-control"
                    name="phone"
                    value={gender === "male" ? "Male" : "Female"}
                    readOnly
                  />
                </div>

                <div className="mt-4">
                  <Link
                    to={{
                      pathname: "/editprofile",
                      profile: this.state.profile
                    }}
                    className="btn btn-info"
                  >
                    Edit Profile
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </>
      );
    } else {
      return <Loading />;
    }
  }
}

export default ViewProfile;
