import React from "react";
import { Link } from "react-router-dom";
import "./Profile.css";
import CallAPI from "../../../CallAPI";

class EditProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loc: "",
      mobile: "",
      gender: "",
      email: "",
      userName: "",
      message: ""
    };
  }

  componentDidMount() {
    let { location } = this.props;
    if (location && location.profile) {
      let { location: loc, email, userName, mobile, gender } = location.profile;
      this.setState({
        loc: loc,
        mobile: mobile,
        gender: gender,
        email: email,
        userName: userName
      });
    }
  }

  handleChange = event => {
    let { name, value } = event.target;
    this.setState({
      [name]: value
    });
  };

  handleSubmit = event => {
    event.preventDefault();
    const form = event.currentTarget;
    form.classList.add("was-validated");
    if (form.checkValidity()) {
      let { userName, loc, mobile, gender } = this.state;
      let options = {
        method: "POST",
        url: "/users/updateProfile",
        headers: {
          token: localStorage.getItem("access-token")
        },
        data: {
          userName: userName,
          location: loc,
          gender: gender,
          mobile: mobile
        }
      };
      CallAPI(options)
        .then(data => {
          if (data.status !== 200) throw new Error(data.status);
          this.setState({
            message: (
              <span>
                Profile is updated successfully <br />
              </span>
            )
          });
        })
        .catch(error => {
          this.setState({
            message: `Something went wrong : ${error}`
          });
        });
    }
  };

  render() {
    let { loc, email, userName, mobile, gender } = this.state;
    return (
      <>
        <div className="container ">
          <div className="row">
            <div className="main-col col-sm-12 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
              <div className="text-success font-weight-bolder text-center  pt-3">
                {this.state.message}
              </div>
              <form
                noValidate
                className="needs-validation"
                onSubmit={this.handleSubmit}
              >
                <div className="form-group">
                  <label htmlFor="email">Email: </label>
                  <input
                    type="text"
                    className="form-control"
                    name="email"
                    value={email}
                    readOnly
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="userName">Username: </label>
                  <input
                    type="text"
                    className="form-control"
                    name="userName"
                    value={userName}
                    onChange={this.handleChange}
                    required
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="loc">Location: </label>
                  <input
                    type="text"
                    className="form-control"
                    name="loc"
                    value={loc}
                    onChange={this.handleChange}
                    required
                  />
                </div>

                <div className="form-group">
                  <label htmlFor="mobile">Mobile : </label>
                  <input
                    type="text"
                    className="form-control"
                    name="mobile"
                    value={mobile}
                    onChange={this.handleChange}
                    required
                  />
                </div>
                <div className="form-check form-check-inline">
                  <input
                    className="form-check-input"
                    type="radio"
                    name="gender"
                    checked={gender === "male"}
                    onChange={this.handleChange}
                    value="male"
                  />
                  <label className="form-check-label" htmlFor="male">
                    Male
                  </label>
                </div>
                <div className="form-check form-check-inline">
                  <input
                    className="form-check-input"
                    type="radio"
                    name="gender"
                    checked={gender === "female"}
                    onChange={this.handleChange}
                    value="female"
                  />
                  <label className="form-check-label" htmlFor="female">
                    Female
                  </label>
                </div>
                <div className="mt-4">
                  <button type="submit" className="btn btn-info">
                    Save
                  </button>
                  <Link to="/profile" className="btn btn-info ml-2">
                    Back
                  </Link>
                </div>
              </form>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default EditProfile;
