import React from "react";
import Loading from "../../Loading";
import RecipeCard from "../RecipeGrid/RecipeCard";
import CallAPI from "../../../CallAPI";

class MyLikes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      likedRecipes: null
    };
  }

  componentDidMount() {
    this.setLikedRecipes();
  }

  setLikedRecipes = () => {
    let likedRecipes = this.getRecipeArrayFromIds();
    this.setState({
      likedRecipes: likedRecipes
    });
  };

  setLoading = () => {
    this.setState({
      loading: true
    });
  };
  unsetLoading = () => {
    this.setState({
      loading: false
    });
  };

  getRecipeArrayFromIds = () => {
    let allRecipes = JSON.parse(localStorage.getItem("recipes"));
    let likedIds = localStorage.getItem("likes");
    let likeRecipes = [];
    if (likedIds && allRecipes) {
      let arrLikedIds = likedIds.split(",");
      likeRecipes = allRecipes.filter(
        recipe => arrLikedIds.indexOf(recipe.idMeal) !== -1
      );
    }
    return likeRecipes;
  };

  disLikeRecipe = recipe_id => {
    let options = {
      method: "POST",
      url: "/users/toggleDislike",
      data: {
        recipe_id: recipe_id
      },
      headers: {
        token: localStorage.getItem("access-token")
      }
    };
    this.setLoading();
    CallAPI(options)
      .then(data => {
        let { message, data: updated_data } = data.data;
        if (message || message === "success") {
          localStorage.setItem("likes", updated_data.likes);
          localStorage.setItem("dislikes", updated_data.dislikes);
          this.unsetLoading();
          this.setLikedRecipes();
        }
      })
      .catch(error => {
        console.log(error);
      });
  };
  removeFromLiked = recipe_id => {
    let options = {
      method: "POST",
      url: "/users/toggleLike",
      data: {
        recipe_id: recipe_id
      },
      headers: {
        token: localStorage.getItem("access-token")
      }
    };
    this.setLoading();
    CallAPI(options)
      .then(data => {
        let { message, data: updated_data } = data.data;
        if (message || message === "success") {
          localStorage.setItem("likes", updated_data.likes);
          this.unsetLoading();
          this.setLikedRecipes();
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  render() {
    let { likedRecipes } = this.state;
    if (this.state.loading) {
      return <Loading />;
    }
    return (
      <>
        <h2 className="text-center text-info">My Likes</h2>
        <div className="main-content">
          <div className="container-fluid">
            <div className="row justify-content-center">
              {(likedRecipes &&
                likedRecipes.length &&
                likedRecipes.map((recipe, i) => {
                  return (
                    <RecipeCard
                      key={i}
                      recipe_data={recipe}
                      fromPage="liked"
                      removeFromLiked={this.removeFromLiked}
                      disLikeRecipe={this.disLikeRecipe}
                    />
                  );
                })) ||
                "No liked found"}
                  
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default MyLikes;
