import React from "react";
import Loading from "../../Loading";
import RecipeCard from "../RecipeGrid/RecipeCard";
import CallAPI from "../../../CallAPI";

class MyDislikes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      dislikedRecipes: null
    };
  }

  componentDidMount() {
    this.setDislikedRecipes();
  }

  setDislikedRecipes = () => {
    let dislikedRecipes = this.getRecipeArrayFromIds();
    this.setState({
      dislikedRecipes: dislikedRecipes
    });
  };

  setLoading = () => {
    this.setState({
      loading: true
    });
  };
  unsetLoading = () => {
    this.setState({
      loading: false
    });
  };

  getRecipeArrayFromIds = () => {
    let allRecipes = JSON.parse(localStorage.getItem("recipes"));
    let dislikedIds = localStorage.getItem("dislikes");
    let dislikeRecipes = [];
    if (dislikedIds && allRecipes) {
      let arrDislikedIds = dislikedIds.split(",");
      dislikeRecipes = allRecipes.filter(
        recipe => arrDislikedIds.indexOf(recipe.idMeal) !== -1
      );
    }
    return dislikeRecipes;
  };

  likeRecipe = recipe_id => {
    let options = {
      method: "POST",
      url: "/users/toggleLike",
      data: {
        recipe_id: recipe_id
      },
      headers: {
        token: localStorage.getItem("access-token")
      }
    };
    this.setLoading();
    CallAPI(options)
      .then(data => {
        let { message, data: updated_data } = data.data;
        if (message || message === "success") {
          localStorage.setItem("likes", updated_data.likes);
          localStorage.setItem("dislikes", updated_data.dislikes);
          this.unsetLoading();
          this.setDislikedRecipes();
        }
      })
      .catch(error => {
        console.log(error);
      });
  };
  removeFromDisliked = recipe_id => {
    let options = {
      method: "POST",
      url: "/users/toggleDislike",
      data: {
        recipe_id: recipe_id
      },
      headers: {
        token: localStorage.getItem("access-token")
      }
    };
    this.setLoading();
    CallAPI(options)
      .then(data => {
        let { message, data: updated_data } = data.data;
        if (message || message === "success") {
          localStorage.setItem("dislikes", updated_data.dislikes);
          this.unsetLoading();
          this.setDislikedRecipes();
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  render() {
    let { dislikedRecipes } = this.state;
    if (this.state.loading) {
      return <Loading />;
    }
    return (
      <>
        <h2 className="text-center text-info">My Dislikes</h2>
        <div className="main-content">
          <div className="container-fluid">
            <div className="row justify-content-center">
              {(dislikedRecipes &&
                dislikedRecipes.length &&
                dislikedRecipes.map((recipe, i) => {
                  return (
                    <RecipeCard
                      key={i}
                      recipe_data={recipe}
                      fromPage="disliked"
                      removeFromDisliked={this.removeFromDisliked}
                      likeRecipe={this.likeRecipe}
                    />
                  );
                })) ||
                "No disliked found"}
                  
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default MyDislikes;
