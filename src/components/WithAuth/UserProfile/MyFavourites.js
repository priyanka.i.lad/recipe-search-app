import React from "react";
import RecipeCard from "../RecipeGrid/RecipeCard";
import CallAPI from "../../../CallAPI";
import Loading from "../../Loading";

class MyFavourites extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      favourites: null,
      loading: false
    };
  }

  componentDidMount() {
    this.setFavourites();
  }

  setFavourites = () => {
    let favouriteRecipes = this.getRecipeArrayFromIds();
    this.setState({
      favourites: favouriteRecipes
    });
  };

  getRecipeArrayFromIds = () => {
    let allRecipes = JSON.parse(localStorage.getItem("recipes"));
    let favouritesIds = localStorage.getItem("favourites");
    let favouriteRecipes = [];
    if (favouritesIds && allRecipes) {
      let arrFavouritesIds = favouritesIds.split(",");
      favouriteRecipes = allRecipes.filter(
        recipe => arrFavouritesIds.indexOf(recipe.idMeal) !== -1
      );
    }
    return favouriteRecipes;
  };

  setLoading = () => {
    this.setState({
      loading: true
    });
  };
  unsetLoading = () => {
    this.setState({
      loading: false
    });
  };
  removeFromFavourites = recipe_id => {
    let options = {
      method: "POST",
      url: "/users/toggleFavourites",
      data: {
        recipe_id: recipe_id
      },
      headers: {
        token: localStorage.getItem("access-token")
      }
    };
    this.setLoading();
    CallAPI(options)
      .then(data => {
        let { message, data: updated_data } = data.data;
        if (message || message === "success") {
          localStorage.setItem("favourites", updated_data.favourites);
          this.unsetLoading();
          this.setFavourites();
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  render() {
    let { favourites } = this.state;
    if (this.state.loading) {
      return <Loading />;
    }
    return (
      <>
        <h2 className="text-center text-info">My Favourites</h2>
        <div className="main-content">
          <div className="container-fluid">
            <div className="row justify-content-center">
              {(favourites &&
                favourites.length &&
                favourites.map((recipe, i) => {
                  return (
                    <RecipeCard
                      key={i}
                      recipe_data={recipe}
                      fromPage="favourite"
                      removeFromFavourites={this.removeFromFavourites}
                    />
                  );
                })) ||
                "No favourites found"}
                  
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default MyFavourites;
