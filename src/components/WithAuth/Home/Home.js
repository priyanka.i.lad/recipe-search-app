import React from "react";
import "./Home.css";
import MainContent from "../MainContent/MainContent";
import Loading from "../../Loading";
import CallRecipeAPI from "../../../CallRecipeAPI";
import Error from "../../Error/Error";
import axios from "axios";

class Home extends React.Component {
  constructor() {
    super();
    this.state = {
      recipes: null,
      error_message: "",
      username: undefined,
      token: undefined,
      loading: true,
      hasError: false,
      errorMessage: ""
    };
  }
  componentDidMount() {
    this.getAllRecipes();
  }

  setError = message => {
    this.setState({
      hasError: true,
      errorMessage: message
    });
  };

  getAllRecipes = () => {
    // if (!localStorage.getItem("recipes")) {
    //   let options = {
    //     method: "GET",
    //     url: "/search.php?s="
    //   };
    //   CallRecipeAPI(options)
    //     .then(data => {
    //       this.setState({
    //         loading: false
    //       });
    //       if (data && data.meals) {
    //         localStorage.setItem("recipes", JSON.stringify(data.meals));
    //       }
    //     })
    //     .catch(error => {
    //       console.log(error);
    //       this.setError(error.message);
    //     });
    // } else {
    //   this.setState({
    //     loading: false
    //   });
    // }

    //using axios.get --- working
    // axios.get(`https://themealdb.com/api/json/v1/1/search.php?s=`).then(res => {
    //   console.log(res);
    //   this.setState({
    //     loading: false
    //   });
    // });

    //using fetch api --- working
    if (!localStorage.getItem("recipes")) {
      let api_url = "https://themealdb.com/api/json/v1/1/search.php?s=";
      fetch(api_url)
        .then(response => {
          this.search = "";
          this.errorMessage = "";
          return response.json();
        })
        .then(data => {
          this.setState({
            loading: false
          });
          if (data && data.meals) {
            localStorage.setItem("recipes", JSON.stringify(data.meals));
          }
        });
    } else {
      this.setState({
        loading: false
      });
    }
  };
  // componentWillMount() {
  //   if (!localStorage.getItem("username")) {
  //     this.props.history.push("/login");
  //   }
  // }

  render() {
    let { hasError, errorMessage, loading } = this.state;
    if (hasError) {
      return <Error errorMessage={errorMessage} />;
    }
    if (loading) {
      return <Loading />;
    }
    let { match } = this.props;
    return (
      <div>
        <MainContent routes={match} />
      </div>
    );
  }
}

export default Home;
