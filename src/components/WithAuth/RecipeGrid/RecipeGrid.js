import React from "react";
import Error from "../../Error/Error";
import RecipeCard from "./RecipeCard";
import Loading from "../../Loading";
import SearchBar from "./SearchBar";
import CallAPI from "../../../CallAPI";
import { Link } from "react-router-dom";

class RecipeGrid extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      recipes: null,
      loading: false
    };
    this.errorMessage = "";
    this.search = "";
  }

  componentDidMount() {
    this.callF2FApi();
  }

  updateInputText = event => {
    this.search = event.target.value;
  };

  searchRecipe = event => {
    event.preventDefault();
    this.callF2FApi();
  };

  callF2FApi = () => {
    let category = "";
    if (this.props.match && this.props.match.params.category)
      category = this.props.match.params.category;
    let api_url = "";
    if (this.search)
      api_url =
        "https://themealdb.com/api/json/v1/1/search.php?s=" + this.search;
    else if (category)
      api_url =
        "https://www.themealdb.com/api/json/v1/1/filter.php?c=" + category;
    if (api_url) {
      fetch(api_url)
        .then(response => {
          this.search = "";
          this.errorMessage = "";
          this.setState({
            loading: false
          });
          // $("#btnSearch").attr("disabled", false);
          return response.json();
        })
        .then(data => {
          this.onDataReceived(data);
        });
    } else {
      this.loadAllRecipes();
    }
  };

  onDataReceived(data) {
    if (data.meals) {
      this.updateRecipesStateOnLoad(data.meals);
      this.errorMessage = "";
    } else {
      let message = "Something went wrong";
      if (data.error)
        message = data.error === "limit" ? "API limit exceeded" : data.error;
      this.setState({
        recipes: null
      });
      this.errorMessage = message;
    }
  }

  updateRecipesStateOnLoad(recipes) {
    let likes =
      localStorage.getItem("likes") && localStorage.getItem("likes").split(",");
    let dislikes =
      localStorage.getItem("dislikes") &&
      localStorage.getItem("dislikes").split(",");
    let favourites =
      localStorage.getItem("favourites") &&
      localStorage.getItem("favourites").split(",");
    let updated_recipes = recipes.map(recipe => {
      recipe["like"] = likes && likes.indexOf(recipe.idMeal) !== -1;
      recipe["dislike"] = dislikes && dislikes.indexOf(recipe.idMeal) !== -1;
      recipe["favourite"] =
        favourites && favourites.indexOf(recipe.idMeal) !== -1;
      return recipe;
    });
    this.setState({
      recipes: updated_recipes
    });
  }

  loadAllRecipes = () => {
    if (localStorage.getItem("recipes")) {
      this.updateRecipesStateOnLoad(
        JSON.parse(localStorage.getItem("recipes"))
      );
    }
  };

  likeRecipe = recipe_id => {
    this.updateStateAfterAction("like", "dislike", recipe_id);

    let options = {
      method: "POST",
      url: "/users/toggleLike",
      data: {
        recipe_id: recipe_id
      },
      headers: {
        token: localStorage.getItem("access-token")
      }
    };
    CallAPI(options)
      .then(data => {
        let { message, data: updated_data } = data.data;
        if (!message || message !== "success") {
          this.updateStateAfterAction("like", "dislike", recipe_id);
        } else {
          localStorage.setItem("likes", updated_data.likes);
          localStorage.setItem("dislikes", updated_data.dislikes);
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  disLikeRecipe = recipe_id => {
    this.updateStateAfterAction("dislike", "like", recipe_id);

    let options = {
      method: "POST",
      url: "/users/toggleDislike",
      data: {
        recipe_id: recipe_id
      },
      headers: {
        token: localStorage.getItem("access-token")
      }
    };
    CallAPI(options)
      .then(data => {
        let { message, data: updated_data } = data.data;
        if (!message || message !== "success") {
          this.updateStateAfterAction("dislike", "like", recipe_id);
        } else {
          localStorage.setItem("likes", updated_data.likes);
          localStorage.setItem("dislikes", updated_data.dislikes);
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  addToFavourites = recipe_id => {
    this.updateStateAfterAction("favourite", "", recipe_id);

    let options = {
      method: "POST",
      url: "/users/toggleFavourites",
      data: {
        recipe_id: recipe_id
      },
      headers: {
        token: localStorage.getItem("access-token")
      }
    };
    CallAPI(options)
      .then(data => {
        let { message, data: updated_data } = data.data;
        if (!message || message !== "success") {
          this.updateStateAfterAction("favourite", "", recipe_id);
        } else {
          localStorage.setItem("favourites", updated_data.favourites);
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  updateStateAfterAction(action, counterAction, recipe_id) {
    let recipeToUpdate = this.state.recipes.find(
      recipe => recipe.idMeal === recipe_id
    );
    recipeToUpdate[action] = recipeToUpdate[action] ? false : true;
    if (
      counterAction &&
      recipeToUpdate[action] &&
      recipeToUpdate[counterAction]
    )
      recipeToUpdate[counterAction] = false;
    let updated_recipes = this.state.recipes.map(recipe => {
      if (recipe_id === recipe.idMeal) {
        recipe = recipeToUpdate;
      }
      return recipe;
    });
    this.setState({
      recipes: updated_recipes
    });
  }

  clearFilter = () => {
    this.callF2FApi();
  };

  render() {
    let { recipes, loading } = this.state;
    if (loading) {
      return <Loading />;
    }
    if (recipes) {
      return (
        <div>
          <SearchBar
            updateInputText={this.updateInputText}
            searchRecipe={this.searchRecipe}
            clearFilter={this.clearFilter}
          />
          <div className="text-center p-2">
            <Link
              to="/recipes"
              className="btn btn-success text-white"
              onClick={this.loadAllRecipes}
            >
              VIEW ALL RECIPES
            </Link>
          </div>
          <div className="main-content">
            <div className="container-fluid">
              <div className="row justify-content-center">
                {(recipes.length &&
                  recipes.map(item => {
                    return (
                      <RecipeCard
                        key={item.idMeal}
                        recipe_data={item}
                        fromPage="grid"
                        likeRecipe={this.likeRecipe}
                        disLikeRecipe={this.disLikeRecipe}
                        addToFavourites={this.addToFavourites}
                      />
                    );
                  })) ||
                  "No recipes found"}
              </div>
            </div>
          </div>
        </div>
      );
    } else if (this.errorMessage) {
      return (
        <div>
          <Error errorMessage={this.errorMessage} />
        </div>
      );
    } else {
      return <></>;
    }
  }
}

export default RecipeGrid;
