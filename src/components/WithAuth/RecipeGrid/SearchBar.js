import React from "react";

function SearchBar(props) {
  let _search;

  return (
    <form
      className="form-inline my-2 my-lg-0 justify-content-center"
      onSubmit={props.searchRecipe}
    >
      <input
        className="form-control mr-sm-2"
        type="text"
        placeholder="Search"
        aria-label="Search"
        ref={input => (_search = input)}
        onChange={props.updateInputText}
      />
      <button
        className="btn btn-info my-2 my-sm-0"
        id="btnSearch"
        type="submit"
      >
        Search
      </button>
      <button
        className="btn btn-secondary m-2  my-sm-0"
        id="btnClear"
        type="reset"
        onClick={props.clearFilter}
      >
        Clear
      </button>
    </form>
  );
}

export default SearchBar;
