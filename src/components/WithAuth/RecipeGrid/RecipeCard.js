import React from "react";
import "./RecipeCard.css";
import { Link } from "react-router-dom";
import Actions from "../Actions";

class RecipeCard extends React.Component {
  render() {
    let {
      strMealThumb,
      strMeal,
      //publisher,
      idMeal,
      like,
      dislike,
      favourite
    } = this.props.recipe_data;

    let {
      fromPage,
      likeRecipe,
      disLikeRecipe,
      addToFavourites,
      removeFromFavourites,
      removeFromLiked,
      removeFromDisliked
    } = this.props;
    let icons = "";
    if (fromPage === "grid") {
      icons = (
        <Actions
          recipe_id={idMeal}
          like={like}
          dislike={dislike}
          favourite={favourite}
          likeRecipe={likeRecipe}
          disLikeRecipe={disLikeRecipe}
          addToFavourites={addToFavourites}
        ></Actions>
      );
    } else if (fromPage === "favourite") {
      icons = (
        <span className="float-right">
          <button
            className="btn btn-link p-0"
            onClick={() => removeFromFavourites(idMeal)}
          >
            <i
              className="fa fa-trash fa-2x delete-icon"
              aria-hidden="true"
              title="Delete this recipe from wishlist"
            />
          </button>
        </span>
      );
    } else if (fromPage === "liked") {
      icons = (
        <span className="float-right">
          <button
            className="btn btn-link p-0"
            onClick={() => disLikeRecipe(idMeal)}
          >
            <i
              className="fa fa-thumbs-o-down fa-2x dislike-icon"
              aria-hidden="true"
              title="Dislike this recipe"
            />
          </button>
          <button
            className="btn btn-link p-0"
            onClick={() => removeFromLiked(idMeal)}
          >
            <i
              className="fa fa-trash fa-2x ml-2 delete-icon"
              aria-hidden="true"
              title="Delete this recipe from liked list"
            />
          </button>
        </span>
      );
    } else if (fromPage === "disliked") {
      icons = (
        <span className="float-right">
          <button
            className="btn btn-link p-0"
            onClick={() => likeRecipe(idMeal)}
          >
            <i
              className="fa fa-thumbs-o-up fa-2x ml-2 like-icon"
              aria-hidden="true"
              title="Like this recipe"
            />
          </button>
          <button
            className="btn btn-link p-0"
            onClick={() => removeFromDisliked(idMeal)}
          >
            <i
              className="fa fa-trash fa-2x ml-2 delete-icon"
              aria-hidden="true"
              title="Delete this recipe from unliked list"
            />
          </button>
        </span>
      );
    }
    return (
      <div className="card p-0 m-3 shadow col-sm-6 col-md-5 col-lg-3">
        <img
          className="card-img-top img-fluid"
          src={strMealThumb}
          alt={strMeal}
        />
        <div className="card-block">
          <div className="card-body">
            <div className="card-title recipe-name h5 text-uppercase">
              {strMeal}
            </div>
            {/* <div className="card-text publisher">
              <span className="font-weight-bold text-uppercase">
                PUBLISHER:
              </span>
              {publisher}
            </div> */}
            <div className="mt-3">
              <Link
                to={{
                  pathname: `/recipe/${idMeal}`,
                  state: {
                    recipe_id: idMeal
                  }
                }}
                className="btn btn-outline-danger"
              >
                VIEW RECIPE
              </Link>

              {icons}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default RecipeCard;
