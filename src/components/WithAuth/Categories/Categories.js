import React from "react";
import { Link } from "react-router-dom";
import allrecipesimg from "../../Images/allrecipes.jpg";

function Categories() {
  return (
    <div className="row">
      <div className="col-lg-12 text-center pb-2">
        <div className="card">
          <img src={allrecipesimg} className="card-img-top" alt="..." />
          <div className="card-body">
            <p className="card-text">
              <Link to="/recipes" className="btn btn-info text-white">
                VIEW ALL RECIPES
              </Link>
            </p>
            <table className="table">
              <tbody>
                <tr>
                  <td>
                    <Link
                      to="/recipes/Chicken"
                      className="btn btn-link text-info"
                    >
                      CHICKEN RECIPES
                    </Link>
                  </td>
                  <td>
                    <Link
                      to="/recipes/Dessert"
                      className="btn btn-link text-info"
                    >
                      DESSERT RECIPES
                    </Link>
                  </td>
                  <td>
                    <Link
                      to="/recipes/Starter"
                      className="btn btn-link text-info"
                    >
                      STARTER RECIPES
                    </Link>
                  </td>
                </tr>
                <tr>
                  <td>
                    <Link to="/recipes/Side" className="btn btn-link text-info">
                      SIDE RECIPES
                    </Link>
                  </td>
                  <td>
                    <Link
                      to="/recipes/Vegan"
                      className="btn btn-link text-info"
                    >
                      VEGAN RECIPES
                    </Link>
                  </td>
                  <td>
                    <Link
                      to="/recipes/Seafood"
                      className="btn btn-link text-info"
                    >
                      SEAFOOD RECIPES
                    </Link>
                  </td>
                </tr>
                <tr>
                  <td>
                    <Link
                      to="/recipes/Pasta"
                      className="btn btn-link text-info"
                    >
                      PASTA RECIPES
                    </Link>
                  </td>
                  <td>
                    <Link
                      to="/recipes/Vegetarian"
                      className="btn btn-link text-info"
                    >
                      VEGETARIAN RECIPES
                    </Link>
                  </td>
                  <td>
                    <Link
                      to="/recipes/Breakfast"
                      className="btn btn-link text-info"
                    >
                      BREAKFAST RECIPES
                    </Link>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div className="col-lg-4 text-center pb-2"></div>
    </div>
  );
}

export default Categories;
