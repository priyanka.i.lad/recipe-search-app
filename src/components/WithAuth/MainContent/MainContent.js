import React from "react";

import Categories from "../Categories/Categories";

function MainContent(props) {
  let categories = "";
  if (props.routes.path === "/") {
    categories = <Categories />;
  }

  return (
    <div>
      <div className="main-content">
        <div className="container">{categories}</div>
      </div>
    </div>
  );
}

export default MainContent;
