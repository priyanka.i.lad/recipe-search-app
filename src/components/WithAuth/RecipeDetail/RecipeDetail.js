import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./RecipeDetail.css";
import Error from "../../Error/Error";
import Loading from "../../Loading";

class RecipeDetail extends Component {
  state = {
    recipe_id: this.props.match.params.id,
    recipe_detail: undefined,
    error_message: "",
    loading: false
  };

  componentDidMount() {
    this.setState({
      loading: true
    });

    let url =
      "https://www.themealdb.com/api/json/v1/1/lookup.php?i=" +
      this.state.recipe_id;
    fetch(url)
      .then(response => {
        this.setState({
          loading: false
        });
        return response.json();
      })
      .then(data => {
        console.log(data.meals[0]);
        if (data.meals[0]) {
          this.setState({
            recipe_detail: data.meals[0],
            error_message: ""
          });
        } else {
          let message = "Something went wrong";
          if (data.error)
            message =
              data.error === "limit" ? "API limit exceeded" : data.error;
          this.setState({
            error_message: message,
            recipe_detail: null
          });
        }
      });
  }

  render() {
    if (this.state.loading) {
      return (
        <div>
          <Loading />
        </div>
      );
    }
    if (this.state.recipe_detail) {
      let {
        strMealThumb,
        strMeal,
        //  publisher,
        strYoutube,
        strInstructions
      } = this.state.recipe_detail;
      let instructions = strInstructions.split("\r\n");
      return (
        <div>
          <div className="container main-content recipe-detail">
            <div className="row">
              <div className="col-md-10">
                <img className="foodimage" src={strMealThumb} alt={strMeal} />
              </div>
              <div className="col-md-10 mt-3">
                <div className="h4 text-uppercase text-danger">{strMeal}</div>
              </div>
              {/* <div className="col-md-10 mt-1">
                <div className="h5 text-uppercase text-secondary">
                  {publisher}
                </div>
              </div> */}
              <div className="col-md-10 mt-1 text-break">
                <p className="h5 font-weight-normal ">
                  Source:{" "}
                  <span>
                    {" "}
                    <a
                      href={strYoutube}
                      target="_blank"
                      rel="noopener noreferrer"
                      className="sourceurl"
                    >
                      {strYoutube}
                    </a>
                  </span>
                </p>
              </div>
              <div className="col-md-10 mt-3 ">
                <ul>
                  {this.state.recipe_detail &&
                    instructions.map((instruction, i) => {
                      return <li key={i}>{instruction}</li>;
                    })}
                </ul>
              </div>
              <div className="col-md-10 mt-3">
                <Link to={`/recipes`} className="btn btn-info">
                  GO HOME
                </Link>
              </div>
            </div>
          </div>
        </div>
      );
    } else return <Error errorMessage={this.state.error_message} />;
  }
}

export default RecipeDetail;
