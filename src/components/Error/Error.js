import React from "react";

function Error(props) {
  return (
    props.errorMessage && (
      <h1 className="text-center text-danger pt-5">
        {" "}
        Error: {props.errorMessage}
      </h1>
    )
  );
}

export default Error;
