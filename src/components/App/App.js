import React from "react";
import { Switch, Route } from "react-router-dom";

import Home from "../WithAuth/Home/Home";
import RecipeDetail from "../WithAuth/RecipeDetail/RecipeDetail";
import Register from "../WithoutAuth/Register/Register";
import ForgotPassword from "../WithoutAuth/ForgotPassword/ForgotPassword";
import Login from "../WithoutAuth/Login/Login";
import RecipeGrid from "../WithAuth/RecipeGrid/RecipeGrid";
import ViewProfile from "../WithAuth/UserProfile/ViewProfile";
import MyFavourites from "../WithAuth/UserProfile/MyFavourites";
import MyLikes from "../WithAuth/UserProfile/MyLikes";
import MyDislikes from "../WithAuth/UserProfile/MyDislikes";
import EditProfile from "../WithAuth/UserProfile/EditProfile";
import VerifyAccount from "../WithoutAuth/VerifyAccount";
import Header from "../Header/Header";

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      isLoggedIn: false
    };
  }

  handleLoginStatus = isLoggedIn => {
    this.setState({
      isLoggedIn: isLoggedIn
    });
  };

  render() {
    const { isLoggedIn } = this.state;
    return (
      <main>
        <Header
          isLoggedIn={isLoggedIn}
          handleLoginStatus={this.handleLoginStatus}
        />
        <Switch>
          <Route
            path="/"
            exact
            render={props =>
              isLoggedIn ? (
                <Home {...props} />
              ) : (
                <Login
                  {...props}
                  isLoggedIn={isLoggedIn}
                  handleLoginStatus={this.handleLoginStatus}
                />
              )
            }
          />
          <Route path="/register" component={Register} />
          <Route
            path="/login"
            render={props => (
              <Login
                {...props}
                isLoggedIn={isLoggedIn}
                handleLoginStatus={this.handleLoginStatus}
              />
            )}
          />
          <Route path="/forgotpswd" component={ForgotPassword} />
          <Route path="/recipe/:id" component={RecipeDetail} />
          <Route path="/recipes/:category?" component={RecipeGrid} />
          <Route path="/profile" component={ViewProfile} />
          <Route path="/editprofile" component={EditProfile} />
          <Route path="/favourites" component={MyFavourites} />
          <Route path="/likes" component={MyLikes} />
          <Route path="/dislikes" component={MyDislikes} />
          <Route path="/verifyaccount" component={VerifyAccount} />
        </Switch>
        {/* </nav> */}
      </main>
    );
  }
}

export default App;
