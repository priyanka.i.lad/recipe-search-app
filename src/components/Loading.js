import React from "react";

function Loading() {
  return (
    <div className="main-content">
      <div className="container-fluid text-center">
        <i
          className="fa fa-spinner fa-spin"
          style={{ fontSize: "80px", color: "forestgreen" }}
        />
      </div>
    </div>
  );
}
export default Loading;
